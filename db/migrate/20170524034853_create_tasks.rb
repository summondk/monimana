class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.date :log_date
      t.integer :creator_id
      t.integer :pic_id
      t.text :detail
      t.text :note
      t.string :name
      t.integer :status, default: 0

      t.integer :business_report_id

      t.timestamps
    end
  end
end
