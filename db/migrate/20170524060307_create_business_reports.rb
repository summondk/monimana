class CreateBusinessReports < ActiveRecord::Migration[5.0]
  def change
    create_table :business_reports do |t|
      t.integer :month
      t.integer :year
      t.bigint :balance

      t.timestamps
    end
  end
end
