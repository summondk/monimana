class CreateBusinessOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :business_operations do |t|
      t.string :name
      t.text :note
      t.integer :business_idea_id

      t.timestamps
    end
  end
end
