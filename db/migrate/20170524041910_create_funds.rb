class CreateFunds < ActiveRecord::Migration[5.0]
  def change
    create_table :funds do |t|
      t.bigint :amount
      t.integer :perform_user_id
      t.date :log_date
      t.text :note
      t.string :detail

      t.timestamps
    end
  end
end
