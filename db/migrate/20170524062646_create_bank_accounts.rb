class CreateBankAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :bank_accounts do |t|
      t.string :name
      t.string :card_no
      t.string :acc_number
      t.string :csc
      t.string :ib_acc
      t.string :ib_password
      t.integer :bank_id
      t.integer :user_id
      t.integer :card_type, default: 0
      t.date :opened_at
      t.date :expired_at
      t.bigint :balance

      t.timestamps
    end
  end
end
