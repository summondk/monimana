class CreateExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :expenses do |t|
      t.integer :log_user_id
      t.integer :perform_user_id
      t.text :note
      t.string :detail
      t.integer :cleverage_id
      t.integer :expense_type_id
      t.bigint :amount
      t.integer :business_operation_id
      t.integer :business_report_id
      t.date :log_date

      t.timestamps
    end
  end
end
