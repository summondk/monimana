class CreateRevenues < ActiveRecord::Migration[5.0]
  def change
    create_table :revenues do |t|
      t.date :log_date
      t.bigint :amount
      t.integer :log_user_id
      t.integer :cleverage_id
      t.integer :business_operation_id
      t.text :note
      t.string :detail
      t.integer :business_report_id

      t.timestamps
    end
  end
end
