class CreateBusinessIdeas < ActiveRecord::Migration[5.0]
  def change
    create_table :business_ideas do |t|
      t.text :detail
      t.text :note
      t.string :name
      t.integer :creator_id

      t.timestamps
    end
  end
end
