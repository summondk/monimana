class CreateBalances < ActiveRecord::Migration[5.0]
  def change
    create_table :balances do |t|
      t.bigint :total_fund
      t.bigint :total_balance

      t.timestamps
    end
  end
end
