'use strict';

(function() {
  angular.module('MainApp', ['ui.bootstrap', 'commonLib'])
    .config(['$httpProvider', '$locationProvider', '$qProvider', defaultConfig]);

  angular.module('MainApp').controller('baseModalCtrl', baseModalCtrl);
  baseModalCtrl.$inject = ['$uibModalInstance'];

  function baseModalCtrl($uibModalInstance) {
    this.ok = function(resolve) {
      $uibModalInstance.close(resolve);
    };

    this.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
})();
