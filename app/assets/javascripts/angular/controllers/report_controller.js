'use strict';
angular.module('MainApp').controller('reportCtrl', reportCtrl);
reportCtrl.$inject = ['common', 'revenueService', 'expenseService'];

function reportCtrl(common, revenueService, expenseService){
  var vm = this;
  vm.data = {};
  vm.init = function(options) {
    vm.data.options = options;
    console.log(options);
  }

  vm.fetchRevenues = function(params) {
    revenueService.fetch(params).then(function(res){

    })
  };

  vm.fetchExpenses = function() {
    revenueService.fetch(params).then(function(res){

    })
  };

  vm.openRevenueModal = function() {
    var modalCtrl = function($controller, $uibModalInstance) {
      var svm = this;
      svm.data = vm.data.options;
      angular.extend(svm, $controller('baseModalCtrl', {$uibModalInstance: $uibModalInstance}));
      svm.revenueConfig = {
        dpOpened: false,
        openDp: function(){
          this.dpOpened = !this.dpOpened;
        }
      },
      svm.expenseConfig = {
        dpOpened: false,
        openDp: function(){
          this.dpOpened = !this.dpOpened;
        }
      }
    };
    var resolve = {
      data : function(){
        return vm.data.options;
      }
    };
    var callback = function(revenue) {
      console.log(revenue);
      revenueService.create(revenue).then(function(res){
        vm.fetchRevenues();
      });
    };
    common.openModal('revenue.html', modalCtrl, 'lg', resolve, callback);
  }
}
