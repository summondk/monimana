'use strict';

angular.module('MainApp')
  .factory('expenseService', ['common', expenseService]);

function expenseService(common) {
  return {
    update: function(params){
      var url = "api/v1/expenses/update";
      return common.ajaxCall('PATCH', url, params);
    },
    create: function(params){
      var url = "api/v1/expenses/create";
      return common.ajaxCall('POST', url, params);
    },
    delete: function(params){
      var url = "api/v1/expenses/update";
      return common.ajaxCall('PUT', url, params);
    },
    fetch: function(params){
      var url = "api/v1/expenses/";
      return common.ajaxCall('GET', url, params);
    }
  }
}
