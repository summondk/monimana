'use strict';

angular.module('MainApp')
  .factory('revenueService', ['common', revenueService]);

function revenueService(common) {
  return {
    update: function(params){
      var url = "api/v1/revenues/update";
      return common.ajaxCall('PATCH', url, params);
    },
    create: function(params){
      var url = "api/v1/revenues/create";
      return common.ajaxCall('POST', url, params);
    },
    delete: function(params){
      var url = "api/v1/revenues/update";
      return common.ajaxCall('PUT', url, params);
    },
    fetch: function(params){
      var url = "api/v1/revenues/";
      return common.ajaxCall('GET', url, params);
    }
  }
}
