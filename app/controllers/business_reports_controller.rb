class BusinessReportsController < ApplicationController
  def index
    @options = {
      business_operations: BusinessOperation.all,
      cleverages: Cleverage.all
    }
  end
end
