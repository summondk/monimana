Rails.application.routes.draw do
  devise_for :users
  root to: "dashboard#index"

  get "reports/" , to: "business_reports#index"

  namespace :api do
    namespace :v1, defaults: {format: "json"}, only: [:create, :update, :index] do
      resources :revenues
      resources :expenses
    end
  end
end
